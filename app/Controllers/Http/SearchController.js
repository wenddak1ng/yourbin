'use strict'

const Documents = use('App/Models/Document')

class SearchController {
  async index({request, response, view}) {
    const data = await request.only(['search_input'])
    const documents = await Documents
      .query()
      .whereRaw(`content LIKE '%${data.search_input}%'`).andWhere('visibility', 'public')
      .orWhereRaw(`title LIKE '%${data.search_input}%'`).andWhere('visibility', 'public')
      .fetch()

    return view.render('page.results', {
      documents: documents.toJSON(),
      label: data.search_input
    })
  }
}

module.exports = SearchController
