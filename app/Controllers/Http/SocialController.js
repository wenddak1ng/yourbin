'use strict'

const User = use('App/Models/User')

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with socials
 */
class SocialController {
  /**
   * Show a list of all socials.
   * GET socials
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new social.
   * GET socials/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ session, response, auth, ally }) {
    const password = 'null'
    try {
      const api = await ally.driver('instagram').getUser()
      const userDetails = {
        email: api.getNickname(),
        password: password,
      }
      const authDetails = {
        email: api.getNickname()
      }
      const user = await User.findOrCreate(authDetails, userDetails)

      await auth.login(user)
      session.flash({ success: 'Вы успешно вошли с помощью Instagram' })
      return response.redirect('back')
    } catch (error) {
      session.flash({ error: 'Произошла ошибка. Возможно, в аккаунте Instagram не указан E-Mail' })
      console.log(error)
      return response.redirect('back')
    }
  }

  /**
   * Create/save a new social.
   * POST socials
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single social.
   * GET socials/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ ally }) {
    await ally.driver('instagram').redirect()
  }

  /**
   * Render a form to update an existing social.
   * GET socials/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update social details.
   * PUT or PATCH socials/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a social with id.
   * DELETE socials/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = SocialController
