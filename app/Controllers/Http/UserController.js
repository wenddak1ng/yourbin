'use strict'

const User = use('App/Models/User')

class UserController {
  async create ({ request, session, response, auth }) {
    const { email, password } = await request.all()
    let user = null

    try {
      user = await User.create({ email, password })
    } catch {
      session.flash({ error: 'Пользователь с таким E-Mail или паролем уже зарегистрирован' })
      return response.redirect('back')
    }
    await auth.login(user)
    await session.flash({ success: 'Вы успешно зарегистрировались' })

    return response.redirect('back')
  }

  async show ({ session, request, response, auth }) {
    const { email, password } = await request.all()
    
    try {
      const user = await User.findBy({ email, password })
      await auth.login(user)
    } catch {
      session.flash({ error: 'Пользователь с таким E-Mail и паролем не найден' })
      return response.redirect('back')
    }
    await session.flash({ success: 'Вы успешно вошли' })

    return response.redirect('back')
  }

  async update ({ session, request, response, auth }) {
    const user = await auth.getUser()
    const { password_old, password } = request.all()

    if(password_old !== user.password) {
      session.flash({ error: 'Текущий пароль не совпадает' })
      return response.redirect('back')
    }
    user.password = await password
    await user.save()
    await session.flash({ success: 'Пароль успешно обновлён' })
    await auth.logout()

    return response.redirect('back')
  }

  async destroy ({ params, request, response, auth }) {
    await auth.logout()

    return response.redirect('/')
  }
}

module.exports = UserController
