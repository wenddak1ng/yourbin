'use strict'

const Settings = use('App/Models/Setting')

class SettingController {
  async update ({ auth, request, response }) {
    const { syntax, visibility, expiration } = await request.except(['_csrf'])
    const syntax_new = await syntax.replace(' selected', '')
    const visibility_new = await visibility.replace(' selected', '')
    const expiration_new = await expiration.replace(' selected', '')
    const settings = await Settings.findBy({ user_id: auth.user.id })

    settings.syntax = syntax_new
    settings.visibility = visibility_new
    settings.expired = expiration_new
    await settings.save()

    response.redirect('back')
  }
}

module.exports = SettingController
