'use strict'

const Document = use('App/Models/Document')
const database = use('Database')

class DocumentController {
  async index ({ auth, view, request }) {
    const documents = await auth.user.documents().fetch()
    const settings = await auth.user.settings().fetch()
    const categories = await database
      .query()
      .select('*')
      .from('categories')
      .orderBy('id', 'asc')
    const views = await database
      .query()
      .select('*')
      .from('views')
      .orderBy('id', 'asc')
    const syntaxes = await database
      .query()
      .select('*')
      .from('syntaxes')
      .orderBy('id', 'asc')

    return view.render('page.documents', {
      documents: documents.toJSON(),
      categories: categories,
      views: views,
      syntaxes: syntaxes,
      settings: settings
    })
  }

  async create ({ auth, view }) {
    const documents = await Document.query().where('visibility', 'public').limit(10).fetch()
    const categories = await database
      .query()
      .select('*')
      .from('categories')
      .orderBy('id', 'asc')
    const views = await database
      .query()
      .select('*')
      .from('views')
      .orderBy('id', 'asc')
    const syntaxes = await database
      .query()
      .select('*')
      .from('syntaxes')
      .orderBy('id', 'asc')
    let yourbins
    let settings
    let pull

    if(auth.user) {
      pull = await auth.user.documents().limit(10).fetch()
      yourbins = pull.toJSON()
      pull = await auth.user.settings().fetch()
      settings = pull.toJSON()
    }

    return view.render('page.home', {
      date: new Date(),
      yourbins: yourbins,
      documents: documents.toJSON(),
      categories: categories,
      views: views,
      syntaxes: syntaxes,
      settings: settings
    })
  }

  async store ({ request, response, auth, session }) {
    let { content, syntax, expiration, visibility, title, _csrf } = request.all()
    const author_id = (auth.user) ? auth.user.id : 1

		if (title.length > 20) {
			session.flash({ error: 'Длина названия не должна превышать 20 символов' })
			return response.redirect('back')
		}

    switch (expiration) {
      case '10min':
        expiration = new Date(Date.now() + 600 * 1000)
        break
      case 'hour':
        expiration = new Date(Date.now() + 3600 * 1000)
        break
      case 'day':
        expiration = new Date(Date.now() + 86400 * 1000)
        break
      case 'week':
        expiration = new Date(Date.now() + 604800 * 1000)
        break
      case 'month':
        expiration = new Date(Date.now() + 2721600 * 1000)
        break
      case 'half-year':
        expiration = new Date(Date.now() + 15778800 * 1000)
        break
      case 'year':
        expiration = new Date(Date.now() + 31557600 * 1000)
        break
      default:
        expiration = new Date(Date.now() + 999999999999 * 1000)
        break
    }
    if (!auth.user && visibility === 'private') {
      session.flash({ error: 'Приватный доступ доступен только авторизованным пользователям' })
      return response.redirect('back')
    }
    try {
      await Document.create({
        user_id: author_id,
        visibility: visibility,
        syntax: syntax,
        expired_at: expiration,
        content: content,
        title: title,
        shorten: _csrf,
      })
    } catch (e) {
      console.log(e)
    }
    await session.flash({ success: 'Документ успешно создан' })

    return response.redirect('/documents/' + _csrf)
  }

  async show ({ params, request, response, view, auth, session }) {
    const categories = await database
      .query()
      .select('*')
      .from('categories')
      .orderBy('id', 'asc')
    const views = await database
      .query()
      .select('*')
      .from('views')
      .orderBy('id', 'asc')
    const syntaxes = await database
      .query()
      .select('*')
      .from('syntaxes')
      .orderBy('id', 'asc')
    const originalUrl = await request.originalUrl()
    const shorten = originalUrl.substring(11)
    const url = request.protocol() + '://' + request.hostname() + ':8000' + originalUrl
    let user = null
    let settings
    if (auth.user) {
      settings = await auth.user.settings().fetch()
    }
    try {
      user = await auth.getUser()
    } catch {}

    try {
      const document = await Document.findBy({ shorten })
      if ((user === null || document.user_id !== user.id) && document.visibility === 'private') {
        session.flash({ error: 'У вас нет доступа к этому документу' })
        return response.redirect('back')
      }
      if(new Date(Date.now()) > document.expired_at) {
        session.flash({ error: 'Срок действия данного документа истёк' })
        return response.redirect('back')
      }
      return view.render('page.document', {
        document: document,
        url: url,
        shorten: shorten,
        categories: categories,
        views: views,
        syntaxes: syntaxes,
        settings: settings
      })
    } catch (e) {
      console.log(e)
    }
  }

  async edit ({ params, request, response, view }) {
  }

  async update ({ params, request, response }) {
  }

  async destroy ({ session, request, response }) {
    const originalUrl = await request.originalUrl()
    const shorten = originalUrl.slice(19, -1)
    const document = await Document.findBy({ shorten: shorten })

    await document.delete()
    await session.flash({ success: 'Документ успешно удалён' })

    return response.redirect('/')
  }
}

module.exports = DocumentController
