'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Setting extends Model {
  setting () {
    return this.belongsTo('App/Models/User', 'user_id')
  }
}

module.exports = Setting
