'use strict'

/** @type {import('@adonisjs/framework/src/Hash')} */
/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class User extends Model {
  static boot () {
    super.boot()

    /**
     * A hook to hash the user password before saving
     * it to the database.
     */
    this.addHook('afterCreate', async (user) => {
      if (user) {
        try {
          user.settings().create({ user_id: user.id })
        } catch (error) {
          console.log(error)
        }
      }
    })
  }

  /**
   * A relationship on tokens is required for auth to
   * work. Since features like `refreshTokens` or
   * `rememberToken` will be saved inside the
   * tokens table.
   *
   * @method tokens
   *
   * @return {Object}
   */
  tokens () {
    return this.hasOne('App/Models/Token')
  }

  documents () {
    return this.hasMany('App/Models/Document')
  }

  settings () {
    return this.hasOne('App/Models/Setting')
  }
}

module.exports = User
