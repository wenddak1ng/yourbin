'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')
const Hash = use('Hash')

class Document extends Model {
	document () {
		return this.belongsTo('App/Models/User', 'user_id')
	}
}

module.exports = Document
