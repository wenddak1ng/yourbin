FROM node:11
LABEL maintainer="wendY <wendy@ggrp.ru>"
RUN npm i -g @adonisjs/cli
WORKDIR /var/www
COPY start.sh /start.sh
RUN chmod 755 /start.sh
CMD ["/start.sh"]