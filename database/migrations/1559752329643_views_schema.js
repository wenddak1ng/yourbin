'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ViewsSchema extends Schema {
  up () {
    this.create('views', (table) => {
      table.increments()
      table.string('type')
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('views')
  }
}

module.exports = ViewsSchema
