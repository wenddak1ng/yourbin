'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class SyntaxSchema extends Schema {
  up () {
    this.create('syntaxes', (table) => {
      table.increments()
      table.string('type')
      table.string('name')
      table.timestamps()
    })
  }

  down () {
    this.drop('syntaxes')
  }
}

module.exports = SyntaxSchema
