'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DocumentSchema extends Schema {
  up () {
    this.create('documents', (table) => {
      table.increments()
      table.integer('user_id').references('id').inTable('users')
      table.string('visibility')
      table.string('syntax')
      table.timestamp('expired_at', { useTz: false })
      table.text('content')
      table.string('shorten')
      table.string('title')
      table.timestamp('created_at', { useTz: false })
      table.timestamp('updated_at', { useTz: false })
    })
  }

  down () {
    this.drop('documents')
  }
}

module.exports = DocumentSchema
