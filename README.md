# Adonis fullstack application

## Environment
#### Из `.gitignore` специально убран `.env`
#### Открываем `.env` и пишем свои данные для коннекта к базе данных

## Setup & run
### NodeJS = `v11.7.0`
### NPM = `6.9.0`
```
git clone https://gitlab.com/wenddak1ng/yourbin.git
cd yourbin
npm i
adonis serve --dev
```
#### Приложение запустится в режиме отладки
#### `Либо запускаемся через докер`

## Database
### Для генерации таблиц делаем следующее
```
cd yourbin
adonis migration:run
```
Сервер постгрес должен быть запущен и в `.env` указаны все данные для коннекта (БД создаём заранее)
#### `Если создавались таблицы через миграции, то обязательно создайте пользователя null@null с паролем null. Так называемый пользователь "Гость"`

# Enjoy this solution