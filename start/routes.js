'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/', 'DocumentController.create')
Route.post('/', 'DocumentController.store')
Route.get('documents/:hash', 'DocumentController.show')
Route.post('search', 'SearchController.index')

Route.group(() => {
	Route.get('auth/:provider', 'SocialController.show')
	Route.get('authenticated/:provider', 'SocialController.create')
	Route.get('auth', 'UserController.show')
	Route.get('reg', 'UserController.create')
}).middleware(['guest'])

Route.group(() => {
	Route.get('documents', 'DocumentController.index')
	Route.get('logout', 'UserController.destroy')
	Route.get('update', 'UserController.update')
	Route.post('settings', 'SettingController.update')
	Route.get('documents/destroy/:hash', 'DocumentController.destroy')
}).middleware(['auth'])